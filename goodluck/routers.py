from rest_framework import routers as drf_routers


class GenericRouter(drf_routers.DefaultRouter):
    def get_default_base_name(self, viewset):
        queryset = getattr(viewset, 'queryset', None)
        assert queryset is not None, (
            '`base_name` argument not specified, and could '
            'not automatically determine the name from the viewset, as '
            'it does not have a `.queryset` attribute.'
        )
        try:
            return queryset._document.__name__.lower()
        except AttributeError:
            return queryset.model._meta.object_name.lower()
