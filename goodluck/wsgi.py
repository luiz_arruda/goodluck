"""
WSGI config for goodluck project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os
import sys
from django.core.wsgi import get_wsgi_application
from whitenoise.django import DjangoWhiteNoise

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

try:
    virtenv = os.environ['OPENSHIFT_PYTHON_DIR'] + '/virtenv/'
except KeyError:
    virtenv = os.path.join(BASE_DIR, "env/")

os.environ['PYTHON_EGG_CACHE'] = os.path.join(virtenv, 'lib/python2.7/site-packages')
virtualenv = os.path.join(virtenv, 'bin/activate_this.py')

try:
    execfile(virtualenv, dict(__file__=virtualenv))
except IOError:
    pass

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "goodluck.settings")

application = get_wsgi_application()
application = DjangoWhiteNoise(application)
