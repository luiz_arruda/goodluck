from __future__ import unicode_literals

from django.contrib.auth.models import *
from mongoengine import *
from django.db import models


class UserTransaction(models.Model):
    user = models.ForeignKey(User)
    description = models.TextField(blank=False, null=False)
    value = models.FloatField(blank=False, null=False)
    date = models.DateTimeField(default=timezone.now)
    approved = models.BooleanField(default=False)


class Bet(EmbeddedDocument):
    """
    Apostas
    """
    numbers = ListField(IntField())
    punters = ListField(GenericReferenceField(User))
    hits = IntField()
    prize = FloatField()
    paid = BooleanField(default=False)


class Competition(EmbeddedDocument):
    """
    Competicoes
    """
    number = IntField(required=True)
    date = DateTimeField(required=True)
    drawn = ListField(IntField())
    bets = ListField(ReferenceField('Bet'))


class Modality(Document):
    """
    Modalidades
    """
    name = StringField(required=True)
    url_check = StringField(required=False)
    options = MapField(FloatField())
    minimum_amount_success = IntField(required=True)
    competitions = ListField(ReferenceField('Competition'))
