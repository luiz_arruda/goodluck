from __future__ import unicode_literals

from api.views import *
from django.conf.urls import url, include
from goodluck.routers import GenericRouter
# from rest_framework.urlpatterns import format_suffix_patterns

app_name = 'api'

routers = GenericRouter()
routers.register(r'users', UserViewSet)
routers.register(r'user-transactions', UserTransactionViewSet)
routers.register(r'groups', GroupViewSet)
routers.register(r'permissions', PermissionViewSet)
routers.register(r'content-types', ContentTypeViewSet)
routers.register(r'modalities', ModalityViewSet)

# routers.register(r'competitions', views.CompetitionViewSet)
# routers.register(r'bets', views.BetViewSet)

urlpatterns = [
    url(r'^api/v1/', include(routers.urls)),
]
