from api.models import *
from django.contrib.auth.hashers import make_password
from rest_framework import serializers as rf_serializers
from rest_framework_mongoengine import serializers


class UserSerializer(rf_serializers.ModelSerializer):
    class Meta:
        model = User
        extra_kwargs = {
            'id': {'read_only': True},
            'password': {'write_only': True},
            'last_login': {'read_only': True},
            'date_joined': {'read_only': True},
            'user_permissions': {'read_only': True},
            'groups': {'read_only': True},
            'is_active': {'read_only': True},
        }
        fields = ['id', 'username', 'first_name', 'last_name', 'email', 'is_active', 'date_joined', 'password',
                  'user_permissions', 'groups', ]

    def create(self, validated_data):
        try:
            validated_data['password'] = make_password(validated_data['password'])
        except KeyError:
            raise rf_serializers.ValidationError({
                'password': ['This field may not be blank.']
            })
        return super(UserSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        try:
            validated_data['password'] = make_password(validated_data['password'])
        except KeyError:
            validated_data['password'] = instance.password
        return super(UserSerializer, self).update(instance, validated_data)


class GroupSerializer(rf_serializers.ModelSerializer):
    class Meta:
        model = Group


class PermissionSerializer(rf_serializers.ModelSerializer):
    class Meta:
        model = Permission


class ContentTypeSerializer(rf_serializers.ModelSerializer):
    class Meta:
        model = ContentType


class UserTransactionSerializer(rf_serializers.ModelSerializer):
    class Meta:
        model = UserTransaction


class BetSerializer(serializers.EmbeddedDocumentSerializer):
    class Meta:
        model = Bet


class CompetitionSerializer(serializers.EmbeddedDocumentSerializer):
    class Meta:
        model = Competition


class ModalitySerializer(serializers.DocumentSerializer):
    class Meta:
        model = Modality
