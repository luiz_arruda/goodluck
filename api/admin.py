from django.contrib import admin
from api.models import UserTransaction


# Register your models here.
class AuthorAdmin(admin.ModelAdmin):
    pass


admin.site.register(UserTransaction, AuthorAdmin)
