from __future__ import print_function

import tempfile
import zipfile
from datetime import datetime

import curly
import os
import re
from htmldom.htmldom import HtmlDom
from unidecode import unidecode


# http://thehtmldom.sourceforge.net/

class LotterySingle(object):
    def __init__(self, modality):
        # http://stackoverflow.com/questions/14594120/python-read-only-property/15812738#15812738
        self.__modality = modality
        self.__draw_number = None
        self.__host = 'http://loterias.caixa.gov.br'
        self.__base_uri = '/wps/portal/loterias/landing/%s/' % self.__modality
        self.__url = self.__host + self.__base_uri
        self.__last_url = None
        self.__last_draw = None  # numero do ultimo concurso
        self.__cookies = ''
        self.__search_form = None
        self.__draw_date = None
        self.__draw_result = []

        self.__bind_draw()
        self.__last_draw = self.__draw_number

    def __bind_draw(self, draw_number=None):
        request_args = self.request_headers
        request_args.extend([
            '-H', 'Cookie: ' + re.sub(r';\s*$', '', 'security=true; ' + self.__cookies)
        ])

        if draw_number is not None:
            assert draw_number <= self.__last_draw
            if draw_number == self.__draw_number:
                return
            self.__url = re.sub(r'dl5/d5/.+$', self.__search_form, self.__url)
            request_args.extend([
                '-H', 'Referer: ' + self.__last_url,
                '-H', 'Content-Type: application/x-www-form-urlencoded',
                '--data', 'concurso=' + str(draw_number),
            ])
        elif self.__last_draw is not None and self.__last_draw == self.__draw_number:
            return

        request_args.insert(0, self.__url)
        response = curly.curl(*request_args)
        response_html = response.read()
        html_dom = HtmlDom().createDom(response_html)

        html_selectors = self.html_selectors
        info = re.compile('.+\s(\d+)\s\((.+)\).*').match(
                html_dom.find(html_selectors['info']).text()
        )

        self.__draw_number = int(info.group(1))
        self.__draw_date = info.group(2)
        self.__search_form = html_dom.find('form#buscaForm').attr('action')
        self.__cookies = re.sub(
                r'( ((Path=/[^,]*)(,|;))|HttpOnly)', '',
                response.getheader('Set-Cookie', self.__cookies)
        )
        self.__url = self.__host + response.getheader('Content-Location', self.__base_uri)
        self.__last_url = self.__url

        result = []

        for selector in html_selectors['result']:
            elem = html_dom.find(selector)
            if elem.length() > 0:
                result.extend([int(elem[idx].text()) for idx in range(elem.length())])
                break

        self.__draw_result = result
        response.close()

    def get_result(self, draw_number=None):
        self.__bind_draw(draw_number=draw_number)
        return LotteryResult(number=self.__draw_number, modality=self.__modality, date=self.__draw_date,
                             result=self.__draw_result)

    @property
    def html_selectors(self):
        return {
            'info': 'div#resultados > div.content-section > div.title-bar > h2 > span',
            'next_draw': '#resultados > div.content-section > div.content > div.resultado-loteria > div.next-prize > p',
            'result': (
                '#resultados > div.content-section > div.content > div.resultado-loteria > table.simple-table > tbody > tr > td',
                '#resultados > div.content-section > div.content > div.resultado-loteria > table.simple-table > tr > td',
                '#resultados > div.content-section > div.content > div.resultado-loteria > ul.numbers > li',
            )
        }

    @property
    def request_headers(self):
        user_agent = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36'
        return [
            '-H', 'Pragma: no-cache',
            '-H', 'Origin: http://loterias.caixa.gov.br',
            '-H', 'Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',
            '-H', 'Upgrade-Insecure-Requests: 1',
            '-H', user_agent,
            '-H', 'Cache-Control: no-cache',
            '-H', 'Accept: text/html',
        ]

    @property
    def host(self):
        return self.__host

    @property
    def base_uri(self):
        return self.__base_uri

    @property
    def url(self):
        return self.__url

    @property
    def last_url(self):
        return self.__last_url

    @property
    def last_draw(self):
        return self.__last_draw

    @property
    def regex_draw_info(self):
        return self.__regex_draw_info

    @property
    def cookies(self):
        return self.__cookies

    @property
    def search_form(self):
        return self.__search_form


class LotteryZip(object):
    def get_results(self, zip_path):
        if re.match(r'^(ht|f)tps?://.+$', zip_path) is None:
            return self.__results_from_local(zip_path)
        else:
            return self.__results_from_remote(zip_path)

    def __results_from_local(self, zip_path):
        results = None
        zip_file = zipfile.ZipFile(zip_path)
        for filename in zip_file.namelist():
            if re.search('\.html?$', filename, re.IGNORECASE):
                html_result = zip_file.read(filename)
                results = self.__generate_results(html_result)
                break
        return results

    def __results_from_remote(self, zip_path):
        response = curly.curl(
                zip_path,
                '-H', 'Cookie: security=true'
        )
        zip_file = tempfile.NamedTemporaryFile(suffix='.zip', mode='wb', delete=False)
        zip_file.write(response.read())
        zip_path = zip_file.name
        zip_file.close()
        results = self.__results_from_local(zip_path)
        os.remove(zip_path)
        return results

    def __generate_results(self, html_result):

        # facilitando a leitura do arquivo por regex
        html_result = re.sub(r'([\r\n\t\s]|&nbsp;?)+', ' ', html_result)
        html_result = re.sub(r'>\s+<', '><', html_result)
        rm_attrs = re.compile(r'<(?P<TAG>\w+?)(\s.*?)>', re.I | re.S | re.L | re.U)
        while re.match(".+" + rm_attrs.pattern + ".+", html_result, rm_attrs.flags):
            html_result = rm_attrs.sub('<\\g<TAG>>', html_result)

        # html_file = tempfile.NamedTemporaryFile(suffix='.html', mode='wb', delete=False)
        # html_file.write(html_result)
        # html_file.close()

        modality = re.search('<title>.+\s([^\s]+)</title>', html_result, re.I).group(1)
        modality = re.sub(r'-', '', unidecode(modality).lower())

        re_is_valid_row = re.compile('^<td>[\d\s]+</td><td>\s*\d{1,2}/\d{1,2}/\d{2,4}\s*</td>.+')
        results = []
        for row in re.finditer(r'<tr>(<td>.+?)</tr>', html_result):
            date = None
            number = None
            result = []
            idx = 0
            if not re_is_valid_row.match(row.group(1)):
                continue
            for cel in re.finditer(r'<td>(.+?)</td>', row.group(1)):
                content = cel.group(1).strip()
                if idx == 0:
                    number = int(content)
                elif idx == 1:
                    date = content
                elif re.match(r'^\d+$', content):
                    result.append(int(content))
                else:
                    break
                idx += 1
            results.append(LotteryResult(modality=modality, number=number, date=date, result=sorted(result)))

        return results


class LotteryResult(object):
    def __init__(self, modality=None, number=None, date=None, result=()):
        self.modality = modality
        self.number = number
        self.date = datetime.strptime(date, '%d/%m/%Y').date()
        self.result = result
