from django.contrib.auth.models import User
from rest_framework import permissions


class UserObjectPermissions(permissions.DjangoObjectPermissions):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return request.user.is_authenticated()

        return isinstance(obj, User) and (obj.id == request.user.id or request.user.is_superuser)

    def has_permission(self, request, view):
        return request.user.is_superuser \
               or (request.method == 'POST' and request.user.is_anonymous()) \
               or (request.method != 'POST' and request.method != 'DELETE' and request.user.is_authenticated())


class IsAdminOrReadOnly(permissions.IsAdminUser):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user.is_superuser

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user.is_superuser


class IsAdminOrOwner(permissions.DjangoObjectPermissions):
    def has_object_permission(self, request, view, obj):
        print('hohoho')
        return obj.user == request.user.id or request.user.is_superuser

    def has_permission(self, request, view):
        print('hehehe')
        return request.user.is_superuser or (request.user.is_authenticated() and request.method != 'PATH')
