from api import serializers
from api.models import *
from api.utils.permissions import UserObjectPermissions, IsAdminOrReadOnly, IsAdminOrOwner
# from django.contrib.auth import models as auth_models
from rest_framework import viewsets as rf_viewsets
from rest_framework.decorators import api_view
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework_mongoengine import viewsets


@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'users': reverse('api:user-list', request=request, format=format),
        'groups': reverse('api:group-list', request=request, format=format),
        'permissions': reverse('api:permission-list', request=request, format=format),
        'content-types': reverse('api:content-type-list', request=request, format=format),
    })


@permission_classes((UserObjectPermissions,))
class UserViewSet(rf_viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = serializers.UserSerializer

    def list(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return Response({
                'detail': 'You do not have permission to perform this action.'
            })
        return super(UserViewSet, self).list(request, *args, **kwargs)

    """
    Retrieve a model instance.
    """

    def retrieve(self, request, *args, **kwargs):
        # TODO restringir a apenas visualizar as proprias informacoes
        return super(UserViewSet, self).retrieve(request, *args, **kwargs)


@permission_classes((IsAdminUser,))
class GroupViewSet(rf_viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = serializers.GroupSerializer


@permission_classes((IsAdminUser,))
class PermissionViewSet(rf_viewsets.ModelViewSet):
    """
    API endpoint that allows permissions to be viewed or edited.
    """
    queryset = Permission.objects.all()
    serializer_class = serializers.PermissionSerializer


@permission_classes((IsAdminUser,))
class ContentTypeViewSet(rf_viewsets.ModelViewSet):
    """
    API endpoint that allows content-types to be viewed or edited.
    """
    queryset = ContentType.objects.all()
    serializer_class = serializers.ContentTypeSerializer


@permission_classes((IsAdminOrOwner,))
class UserTransactionViewSet(rf_viewsets.ModelViewSet):
    queryset = UserTransaction.objects.all()
    serializer_class = serializers.UserTransactionSerializer


@permission_classes((IsAdminOrReadOnly,))
class ModalityViewSet(viewsets.ModelViewSet):
    queryset = Modality.objects.all()
    serializer_class = serializers.ModalitySerializer
    lookup_field = 'name'

# class CompetitionViewSet(viewsets.ModelViewSet):
#     queryset = Competition.objects.all()
#     serializer_class = serializers.CompetitionSerializer
#
#
# class BetViewSet(viewsets.ModelViewSet):
#     queryset = Bet.objects.all()
#     serializer_class = serializers.BetSerializer
