from api.utils.lottery import LotterySingle, LotteryZip
from time import sleep


def test_lottery(sleep_time=5, prev=5):
    for modality in ('lotofacil', 'megasena', 'quina', 'lotomania',):
        lottery = LotterySingle(modality)
        draw_number = lottery.get_result().number
        for i in range(prev):
            lottery_result = lottery.get_result(draw_number - i)
            print(lottery_result.modality, lottery_result.number, lottery_result.date, lottery_result.result)
            sleep(sleep_time)


def test_lottery_zip(sleep_time=5, modality=None):
    modalities = {
        'lotofacil': 'D_lotfac',
        'quina': 'D_quina',
        'lotomania': 'D_lotoma',
        'megasena': 'D_megase',
    }
    url = 'http://www1.caixa.gov.br/loterias/_arquivos/loterias/%s.zip'
    assert modality and modality in modalities
    lottery = LotteryZip()
    for lottery_result in lottery.get_results(url % modalities[modality]):
        print(lottery_result.modality, lottery_result.number, lottery_result.date, lottery_result.result)
        sleep(sleep_time)

# test_lottery()
# test_lottery_zip(modality='megasena')
