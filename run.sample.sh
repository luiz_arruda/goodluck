#!/bin/bash

docker start mysql
docker start mongodb

MONGODB_DB_HOST=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' mongodb)
export OPENSHIFT_MONGODB_DB_URL="mongodb://${MONGODB_DB_HOST}:27017/"

MYSQL_DB_HOST=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' mysql)
export OPENSHIFT_MYSQL_DB_URL="mysql://root:password@${MYSQL_DB_HOST}:3306/"

export PY_ENV=dev

./manage.py $@