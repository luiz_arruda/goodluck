from setuptools import setup

setup(name='goodluck',
      version='0.1',
      description='API para apostas lotericas',
      author='Pedro Silva',
      author_email='pdx.henrique@gmail.com',
      url='http://blog.phenrique.com/',
      install_requires=[
          'Django>=1.8',
          'django-jsonview==0.5.0',
      ],
      dependency_links=[
          'https://pypi.python.org/simple/django/',
          'https://pypi.python.org/simple/django-jsonview/',
      ],
      )
